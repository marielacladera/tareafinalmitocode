import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { Menu } from 'src/app/_model/menu';
import { Rol } from 'src/app/_model/rol';
import { MenuService } from 'src/app/_service/menu.service';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  formMenu: FormGroup;
  id: number;
  roles: Rol[];
  url: string;

  constructor(
    private route: ActivatedRoute,
    private menuService: MenuService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formMenu = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl(''),
      'icono': new FormControl('')
    });

    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.initForm();
    });
  }

  initForm(){
    this.menuService.listarPorId(this.id).subscribe(data =>{
      this.formMenu = new FormGroup({
        'id': new FormControl(data.idMenu),
        'nombre': new FormControl(data.nombre),
        'icono': new FormControl(data.icono)
      });
      this.roles = data.roles;
      this.url = data.url;
    });
  }

  operar(){
    let menu = new Menu();
    menu.idMenu = this.formMenu.value['id'];
    menu.nombre = this.formMenu.value['nombre'];
    menu.icono = this.formMenu.value['icono'];
    menu.roles = this.roles;
    menu.url = this.url;
    this.menuService.modificar(menu).pipe(switchMap(()=>{
      return this.menuService.listar();
    })).subscribe(data => {
      this.menuService.setMenuCambio(data);
      this.menuService.setMensajeCambio('SE MODIFICO');
    });
    this.router.navigate(['/pages/menu']);
  }
}

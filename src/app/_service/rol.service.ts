import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Rol } from '../_model/rol';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class RolService extends GenericService<Rol>{
  private rolesCambio= new Subject<Rol[]>();
  private mensajeCambio = new Subject<string>();

  constructor(http: HttpClient) {
    super(http,
      `${environment.HOST}/roles`)
   }

  listarPageable(p: number, s: number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&size=${s}`);
  }
  
  getRolesCambio(){
    return this.rolesCambio.asObservable();
  }

  setRolesCambio(roles: Rol[]){
    this.rolesCambio.next(roles);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }
}

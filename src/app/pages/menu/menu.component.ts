import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Menu } from 'src/app/_model/menu';
import { MenuService } from 'src/app/_service/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  dataSource: MatTableDataSource<Menu>;
  displayedColumns: string[] = ['nombre', 'icono', 'url','acciones'];
  @ViewChild(MatSort) matSort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator; 
  cantidad: number;

  constructor(
    private serviceMenu: MenuService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.serviceMenu.getMensajeCambio().subscribe(data =>{
      this.snackBar.open(data, 'AVISO',{ duration: 3000})
    });

    this.serviceMenu.getMenuCambio().subscribe(data =>{
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.matSort;
      this.dataSource.paginator = this.paginator;
    });

    this.serviceMenu.listarPageable(0,5).subscribe(data =>{
      this.cantidad = data.totalElements
      this.dataSource = new MatTableDataSource(data.content);
    });
  }

  mostrarMas(e: any){
    this.serviceMenu.listarPageable(e.pageIndex, e.pageSize).subscribe(data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    })
  }

}

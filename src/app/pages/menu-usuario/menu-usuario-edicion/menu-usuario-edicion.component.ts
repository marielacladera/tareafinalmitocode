import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { Rol } from 'src/app/_model/rol';
import { Usuario } from 'src/app/_model/usuario';
import { RolService } from 'src/app/_service/rol.service';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-menu-usuario-edicion',
  templateUrl: './menu-usuario-edicion.component.html',
  styleUrls: ['./menu-usuario-edicion.component.css']
})
export class MenuUsuarioEdicionComponent implements OnInit {

 
  @ViewChild('inputUsuario') public inputUsuario: ElementRef;
  usuario: Usuario = new Usuario();
  rolesMenu: Rol[];

  roles$: Observable<Rol[]>;
  rolesSeleccionados: Rol[] = [];
  rolSeleccionado: Rol;
  id: number;
  edicion: boolean;
  constructor(
    private route: ActivatedRoute,
    private usuarioService: UsuarioService,
    private rolService: RolService,
    private router: Router,
    private snackBar: MatSnackBar

  ) { }

  ngOnInit(): void {
    this.listarInicial();
    this.route.params.subscribe(data =>{
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.llenarDatosMenu();
    });
  }
  
  llenarDatosMenu(){
    this.usuarioService.listarPorId(this.id).subscribe(data =>{
      this.usuario = data;
      this.rolesMenu= data.roles; 
      this.inputUsuario.nativeElement.value = data.username;
    });
  }

  listarInicial(){
    this.roles$ = this.rolService.listar();
  }

  agregarRol(){
    if(this.rolSeleccionado){
      //let contador = 0;
      let res: boolean = false;
      for(let i = 0; i < this.rolesSeleccionados.length; i++){
        let rol = this.rolesSeleccionados[i];
        if(rol.idRol === this.rolSeleccionado.idRol){
          //contador++;
          res = true;
          break;
        }
      }
      if(res == true){
        this.snackBar.open('El rol ya ha sido seleccionado', 'AVISO', {duration: 3000});
      }else{
        this.rolesSeleccionados.push(this.rolSeleccionado);
      }
   }
  }

  removerRol(index: number){
    this.rolesSeleccionados.splice(index, 1);
  }

  aceptar(){
    this.usuario.roles = this.rolesSeleccionados;
    this.usuarioService.modificar(this.usuario).pipe(switchMap(()=>{
      return this.usuarioService.listar();
    })).subscribe(data => {
      this.usuarioService.setMensajeCambio('SE MODIFICO');
      this.usuarioService.setUsuarioCambio(data);
    });
    this.limpiarControles();
    this.router.navigate(['/pages/menu-usuario']);
  }
  limpiarControles(){
    this.rolesSeleccionados = [];
    this.rolSeleccionado = null;
  }
}


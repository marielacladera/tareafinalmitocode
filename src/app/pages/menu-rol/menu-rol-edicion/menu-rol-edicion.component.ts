import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { Menu } from 'src/app/_model/menu';
import { Rol } from 'src/app/_model/rol';
import { MenuService } from 'src/app/_service/menu.service';
import { RolService } from 'src/app/_service/rol.service';

@Component({
  selector: 'app-menu-rol-edicion',
  templateUrl: './menu-rol-edicion.component.html',
  styleUrls: ['./menu-rol-edicion.component.css']
})
export class MenuRolEdicionComponent implements OnInit {

  @ViewChild('inputMenu') public inputMenu: ElementRef;
  menu: Menu = new Menu();
  rolesMenu: Rol[];

  roles$: Observable<Rol[]>;
  rolesSeleccionados: Rol[] = [];
  rolSeleccionado: Rol;
  id: number;
  edicion: boolean;
  constructor(
    private route: ActivatedRoute,
    private menuService: MenuService,
    private rolService: RolService,
    private router: Router,
    private snackBar: MatSnackBar

  ) { }

  ngOnInit(): void {
    this.listarInicial();
    this.route.params.subscribe(data =>{
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.llenarDatosMenu();
    });
  }
  
  llenarDatosMenu(){
    this.menuService.listarPorId(this.id).subscribe(data =>{
      this.menu = data;
      this.rolesMenu= data.roles; 
      this.inputMenu.nativeElement.value = data.nombre;
    });
  }

  listarInicial(){
    this.roles$ = this.rolService.listar();
  }

  agregarRol(){
    if(this.rolSeleccionado){
      //let contador = 0;
      let res: boolean = false;
      for(let i = 0; i < this.rolesSeleccionados.length; i++){
        let rol = this.rolesSeleccionados[i];
        if(rol.idRol === this.rolSeleccionado.idRol){
          //contador++;
          res = true;
          break;
        }
      }
      if(res == true){
        this.snackBar.open('El rol ya ha sido seleccionado', 'AVISO', {duration: 3000});
      }else{
        this.rolesSeleccionados.push(this.rolSeleccionado);
      }
   }
  }

  removerRol(index: number){
    this.rolesSeleccionados.splice(index, 1);
  }

  aceptar(){
    this.menu.roles = this.rolesSeleccionados;
    this.menuService.modificar(this.menu).pipe(switchMap(()=>{
      return this.menuService.listar();
    })).subscribe(data => {
      this.menuService.setMensajeCambio('SE MODIFICO');
      this.menuService.setMenuCambio(data);
    });
    this.limpiarControles();
    this.router.navigate(['/pages/menu-rol']);
  }
  limpiarControles(){
    this.rolesSeleccionados = [];
    this.rolSeleccionado = null;
  }
}

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { map, Observable, switchMap } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { Signo } from 'src/app/_model/signo';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignoService } from 'src/app/_service/signo.service';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {
  @ViewChild('inputPaciente') public inputPaciente: ElementRef;
  id: number;
  edicion: boolean;
  form: FormGroup;
  pacientes: Paciente[];
  maxFecha: Date = new Date();
  pacienteAuxiliar: Paciente; 

  myControlPaciente: FormControl = new FormControl();
  pacientesFiltrados$: Observable<Paciente[]>;

  constructor(
    private route: ActivatedRoute,
    private signoService: SignoService,
    private pacienteService: PacienteService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form =new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoCardiaco': new FormControl('')
    });
    this.listarInicial();
    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });
    this.pacientesFiltrados$ = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));
  }

  initForm(){
    if(this.edicion){
      this.signoService.listarPorId(this.id).subscribe(data => {
        this.form = new FormGroup({
          'id': new FormControl(data.idSignoVital),
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(data.fecha),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoCardiaco': new FormControl(data.ritmoCardiaco),         
        });
        this.pacienteAuxiliar = data.paciente;
        this.inputPaciente.nativeElement.value = data.paciente.nombres;
      });
    }
  }

  filtrarPacientes(val: any){
    if(val != null && val.idPaciente > 0){
      return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.toString().includes(val.ci)
      );  
    }  
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.toString().includes(val)
      );
  }

  mostrarPaciente(val: any){
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  listarInicial(){
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  operar(){
    let signo = new Signo();
    signo.idSignoVital = this.form.value['id'];
    signo.paciente = this.form.value['paciente'];
    signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    signo.pulso = this.form.value['pulso'];
    signo.temperatura = this.form.value['temperatura'];
    signo.ritmoCardiaco = this.form.value['ritmoCardiaco'];
    console.log(signo.paciente);
    if(this.edicion){
      if(!signo.paciente){
        signo.paciente =  this.pacienteAuxiliar;
      }
      this.signoService.modificar(signo).pipe(switchMap(() => {
        return this.signoService.listar();
      })).subscribe(data => {
        this.signoService.setSignoCambio(data);
        this.signoService.setMensajeCambio('SE MODIFICO');
      });
    }else{
      this.signoService.registrar(signo).pipe(switchMap(() => {
        return this.signoService.listar();
      })).subscribe(data =>{
        this.signoService.setSignoCambio(data);
        this.signoService.setMensajeCambio('SE REGISTRO');
      })
    }
    this.router.navigate(['/pages/signo']);
  }
}

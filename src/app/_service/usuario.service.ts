import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Usuario } from '../_model/usuario';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends GenericService<Usuario>{
  private usuarioCambio = new Subject<Usuario[]>();
  private mensajeCambio = new Subject<string>();

  constructor(http: HttpClient) { 
    super(http, 
      `${environment.HOST}/usuarios`);
  }

  getUsuarioCambio(){
    return this.usuarioCambio.asObservable();
  }
  
  setUsuarioCambio(usuario: Usuario[]){
    this.usuarioCambio.next(usuario);
  }

  getMensajeCambio(){
    return this.mensajeCambio.asObservable();
  }

  setMensajeCambio(mensaje: string){
    this.mensajeCambio.next(mensaje);
  }

  listarPageable(p: number, s: number){
    return this.http.get<any>(`${this.url}/pageable?page=${p}&&size=${s}`);
  }

}
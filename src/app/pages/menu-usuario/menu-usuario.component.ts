import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Usuario } from 'src/app/_model/usuario';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-menu-usuario',
  templateUrl: './menu-usuario.component.html',
  styleUrls: ['./menu-usuario.component.css']
})
export class MenuUsuarioComponent implements OnInit {
  dataSource: MatTableDataSource<Usuario>;
  displayedColumns: string[] = ['nombre', 'acciones'];
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  cantidad: number;

  constructor(
    private usuarioService: UsuarioService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.usuarioService.getMensajeCambio().subscribe(data => {
      this.snackBar.open(data, 'AVISO', {duration: 3000});
    });  

    this.usuarioService.getUsuarioCambio().subscribe(data => {
      this.crearTabla(data);
    });

    this.usuarioService.listar().subscribe(data=>{
      this.crearTabla(data);
    });
 
    /*this.usuarioService.listarPageable(0, 5).subscribe(data =>{
      console.log(data);
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });*/
  }
 
  crearTabla(data: Usuario[]){
    this.dataSource = new MatTableDataSource(data);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }
  
  /*mostrarMas(e: any){
    this.usuarioService.listarPageable(e.pageIndex, e.pageSize).subscribe( data => {
      this.cantidad = data.totalElements;
      this.dataSource = new MatTableDataSource(data.content);
    });
  }*/
}

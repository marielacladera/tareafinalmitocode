import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, switchMap } from 'rxjs';
import { Rol } from 'src/app/_model/rol';
import { RolService } from 'src/app/_service/rol.service';

@Component({
  selector: 'app-rol-edicion',
  templateUrl: './rol-edicion.component.html',
  styleUrls: ['./rol-edicion.component.css']
})
export class RolEdicionComponent implements OnInit {

  formRol: FormGroup;
  id: number;

  constructor(
    private route: ActivatedRoute,
    private serviceRol: RolService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.formRol = new FormGroup({
      'id': new FormControl(0),
      'descripcion': new FormControl(''),
      'nombre': new FormControl('')
    })
    this.route.params.subscribe(data => {
      this.id = data['id'];
      this.initForm();
    })
  }

  initForm(){
    this.serviceRol.listarPorId(this.id).subscribe(data => {
      this.formRol = new FormGroup({
        'id': new FormControl(data.idRol),
        'descripcion': new FormControl(data.descripcion),
        'nombre': new FormControl(data.nombre)
      });
    });
  }

  operar(){
    let rol = new Rol();
    rol.idRol = this.formRol.value['id'];
    rol.descripcion = this.formRol.value['descripcion'],
    rol.nombre = this.formRol.value['nombre'];
    this.serviceRol.modificar(rol).pipe(switchMap(() => {
      return this.serviceRol.listar();
    })).subscribe(data =>{
      this.serviceRol.setRolesCambio(data);
      this.serviceRol.setMensajeCambio('SE MODIFICO');
    });
    this.router.navigate(['/pages/rol']);
  }
}
